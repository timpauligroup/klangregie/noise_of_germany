class Button {
  private:
    int pin;
    bool pressed;
  public:
    Button() {
    }
    Button(int pin) {
      this->pin = pin;
      pinMode(pin, INPUT_PULLUP);
      digitalWrite(pin, HIGH);
    }

    void check() {
      if (digitalRead(this->pin) == LOW and not this->pressed) {
        Serial.println(this->pin);
        this->pressed = true;
      }
      else if (digitalRead(this->pin) == HIGH) {
        this->pressed = false;
      }
    }
};

Button buttons[5];

void setup() {
  int startPin = 2;
  int len = sizeof(buttons) / sizeof(buttons[0]);
  for (int i = startPin; i < startPin + len ; i++) {
    buttons[i - startPin] = Button(i);
  }
  Serial.begin(9600);
}

void loop() {
  int len = sizeof(buttons) / sizeof(buttons[0]);
  for (int i = 0; i < len; i++){
    buttons[i].check();
  }
  delay(10);
}
