use crate::Model;
use nannou_osc as osc;

pub fn send_osc_buzzer(model: &Model, number: i32) {
    let osc_addr = "/noise_of_germany/buzzer".to_string();
    let osc_buzzer = vec![osc::Type::Int(number)];
    model.sender.send((&osc_addr, osc_buzzer)).ok();
}

pub fn send_osc_applause(model: &Model, off_on: i32) {
    let osc_addr = "/noise_of_germany/applause".to_string();
    let osc_off_on = vec![osc::Type::Int(off_on)];
    model.sender.send((&osc_addr, osc_off_on)).ok();
}

pub fn send_osc_signal(model: &Model) {
    let osc_addr = "/noise_of_germany/signal".to_string();
    let osc_msg = vec![];
    model.sender.send((&osc_addr, osc_msg)).ok();
}

pub fn send_osc_consecutive_hits(model: &Model, counter_number: i32) {
    let osc_addr = "/noise_of_germany/consecutive_hits".to_string();
    let osc_event = vec![
        osc::Type::Int(counter_number),
        osc::Type::Int(model.consecutive_hits),
    ];
    model.sender.send((&osc_addr, osc_event)).ok();
}

pub fn send_osc_new_leader(model: &Model) {
    let osc_addr = "/noise_of_germany/new_leader".to_string();
    let osc_msg = vec![];
    model.sender.send((&osc_addr, osc_msg)).ok();
}