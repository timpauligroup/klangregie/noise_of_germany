use nannou::prelude::*;
use nannou::text::Font;
use nannou_osc as osc;
use std::io::*;
mod counter;
mod nog_audio;
mod nog_video;
use counter::Counter;

// CONSTANTS
static SERIALPORT_NAME: &str = "/dev/ttyACM0";
//static SERIALPORT_NAME: &str = "/dev/tty0";
static BAUD_RATE: u32 = 9600;
static OSC_PORT: u32 = 9001;
static COUNTER_NAMES: [&str; 5] = ["Torsten", "Bourbon", "Gina", "Will", "Flora"];
static IMAGE_NAMES: [&str; 5] = [
    "torsten_cropped.png",
    "bourbon_cropped.png",
    "gina_cropped.png",
    "will_cropped.png",
    "flora_cropped.png",
];

pub struct Model {
    serial_port: serialport::TTYPort,
    counters: Vec<Counter>,
    font: Font,
    background: wgpu::Texture,
    current_frame: i32,
    visible: bool,
    sender: osc::Sender<osc::Connected>,
    applause: bool,
    last_counter_hit: i32,
    consecutive_hits: i32,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let _window = app
        .new_window()
        .size(1920, 1080)
        .key_pressed(key_pressed)
        .view(view)
        .build()
        .unwrap();

    app.set_loop_mode(LoopMode::rate_fps(25.0));

    let serial_port = serialport::new(SERIALPORT_NAME, BAUD_RATE)
        .open_native()
        .expect("error");

    let assets_path = app.assets_path().unwrap();

    let background = nog_video::load_image(app, 1);
    let mut counters = vec![];
    for (name, img_name) in COUNTER_NAMES.iter().zip(IMAGE_NAMES.iter()) {
        let img_path = assets_path.join("faces").join(img_name);
        let image = wgpu::Texture::from_path(app, img_path).unwrap();
        counters.push(Counter {
            count: 0,
            name: name.to_string(),
            image,
        })
    }

    let font_path = assets_path.join("font").join("entsans.ttf");
    let font: Font = nannou::text::font::from_file(font_path).unwrap();

    let target_addr = format!("{}:{}", "127.0.0.1", OSC_PORT);

    let sender = osc::sender()
        .expect("Could not bind to default socket")
        .connect(target_addr)
        .expect("Could not connect to socket at address");

    Model {
        serial_port,
        counters,
        font,
        background,
        current_frame: 1,
        visible: false,
        sender,
        applause: false,
        last_counter_hit: 6,
        consecutive_hits: 0,
    }
}

fn update(app: &App, model: &mut Model, _update: Update) {
    let mut serial_buffer: Vec<u8> = vec![0; 1];
    match model.serial_port.read(serial_buffer.as_mut_slice()) {
        Ok(_t) => match String::from_utf8_lossy(&serial_buffer).parse::<i32>() {
            Ok(t) => {
                let old_max: String = model
                    .counters
                    .iter()
                    .max_by_key(|c| c.count)
                    .unwrap()
                    .name
                    .clone();
                model.counters[(t - 2) as usize].count += 1;
                let counter_number = t - 1;

                let new_max: String = model
                    .counters
                    .iter()
                    .max_by_key(|c| c.count)
                    .unwrap()
                    .name
                    .clone();

                // logic
                if model.last_counter_hit == counter_number {
                    model.consecutive_hits += 1;
                } else {
                    model.last_counter_hit = counter_number;
                    model.consecutive_hits = 1;
                }
                // play sound
                if new_max != old_max && model.counters[(t - 2) as usize].count > 5 {
                    nog_audio::send_osc_new_leader(model);
                } else {
                    if model.last_counter_hit == counter_number
                        && model.consecutive_hits > 2
                        && model.consecutive_hits < 8
                    {
                        nog_audio::send_osc_consecutive_hits(model, counter_number)
                    } else {
                        nog_audio::send_osc_buzzer(model, counter_number);
                    }
                }
            }
            Err(_e) => (),
        },
        Err(ref e) if e.kind() == std::io::ErrorKind::TimedOut => (),
        Err(e) => eprintln!("{:?}", e),
    }
    if model.current_frame >= 750 {
        model.current_frame = 1;
    } else {
        model.current_frame += 1;
    }
    model.background = nog_video::load_image(app, model.current_frame);
}

fn key_pressed(_app: &App, model: &mut Model, key: Key) {
    match key {
        Key::Space => {
            if model.applause {
                model.applause = false;
                nog_audio::send_osc_applause(model, 0);
            } else {
                model.applause = true;
                nog_audio::send_osc_applause(model, 1);
            }
        }
        Key::S => nog_audio::send_osc_signal(model),
        Key::P => {
            for counter in model.counters.iter() {
                println!("{}", counter.to_string());
            }
            println!("");
        }
        Key::V => {
            model.visible = if model.visible == false { true } else { false };
        }
        Key::Key1 => {
            model.counters[0].count += 1;
        }
        Key::Key2 => {
            model.counters[1].count += 1;
        }
        Key::Key3 => {
            model.counters[2].count += 1;
        }
        Key::Key4 => {
            model.counters[3].count += 1;
        }
        Key::Key5 => {
            model.counters[4].count += 1;
        }
        _ => {}
    }
}

fn view(app: &App, model: &Model, frame: Frame) {
    let draw = app.draw();

    // background video
    draw.texture(&model.background);
    // foreground counters
    if model.visible {
        nog_video::draw_counters(app, model, &draw);
    }

    draw.to_frame(app, &frame).unwrap();
}
