use crate::Model;
use nannou::prelude::*;

pub fn load_image(app: &App, current_frame: i32) -> wgpu::Texture {
    let assets = app.assets_path().unwrap();
    let number = format!("{:0>4}{}", &current_frame.to_string(), ".png");
    let img_path = assets.join("video").join(number);
    let texture = wgpu::Texture::from_path(app, img_path).unwrap();
    texture
}

pub fn draw_counters(app: &App, model: &Model, draw: &Draw) {
    let length = (&model.counters).len() as i32;
    let unit = (app.window_rect().h() * 0.6) / ((length) as f32);
    let ys = (1..(length + 1)).map(|i| (i as f32) * unit);
    for (counter, y_distance) in (&model.counters).iter().zip(ys) {
        let y = app.window_rect().top() - y_distance + (unit / 2.0) - 20.0;
        let x = -(app.window_rect().w() / 4.0);

        draw.texture(&counter.image).x_y(x, y).w_h(unit, unit);

        draw_text(model, draw, &counter.to_string(), 65, WHITE, x, y, unit);

        draw_text(model, draw, &counter.to_string(), 64, BLACK, x, y, unit);
    }
}

fn draw_text(
    model: &Model,
    draw: &Draw,
    text: &str,
    size: u32,
    color: Rgb8,
    x: f32,
    y: f32,
    unit: f32,
) {
    draw.text(&text)
        .color(color)
        .font_size(size)
        .no_line_wrap()
        .left_justify()
        .font(model.font.clone())
        .x_y(x + (1.5 * unit), y);
}
