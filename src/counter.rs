use nannou::prelude::*;

pub struct Counter {
    pub count: i32,
    pub name: String,
    pub image: wgpu::Texture,
}

impl ToString for Counter {
    fn to_string(&self) -> String {
        let text = format!(
            "{}: {}",
            self.name.to_string(),
            self.count.to_string()
        );
        text
    }
}
